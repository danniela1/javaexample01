package deloitte.Academy.lesson01;

import deloitte.Academy.lesson.Arithmetic.Arithmetic;
import deloitte.Academy.lesson.Comparisons.Comparisons;
import deloitte.Academy.lesson.LogicalOperators.LogicalOperators;

/**
 * 
 * @author nnavarrete 
 * Ejecuta los metodos realizados en la clase arithmetic,
 * Comparisons and LogicalOperators.
 */
public class Run {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Arithmetic suma = new Arithmetic();
		Integer num1 = 10;
		Integer num2 = 7;

		Comparisons valor = new Comparisons();
		Integer val1 = 10;
		Integer val2 = 2;

		LogicalOperators val = new LogicalOperators();
		Integer v1 = 8;
		Integer v2 = 8;

		// Aritmetica
		String resultado = ("Suma: " + suma.suma(num1, num2));
		String resultado2 = ("Resta: " + suma.resta(num1, num2));
		String resultado3 = ("Multiplicacion: " + suma.multiplicacion(num1, num2));
		String resultado4 = ("Division: " + suma.division(num1, num2));
		String resultado5 = ("Modulo: " + suma.modulo(num1, num2));

		// Comparadores
		String resultado6 = ("El resultado de " + val1 + " y " + val2 + " es: " + valor.valoresIguales(val1, val2));

		String resultado7 = ("El resultado de " + val1 + " y " + val2 + " es: " + valor.valoresDiferentes(val1, val2));

		String resultado8 = ("El resultado de " + val1 + " y " + val2 + " es: " + valor.menorQue(val1, val2));

		String resultado9 = ("El resultado de " + val1 + " y " + val2 + " es: " + valor.mayorQue(val1, val2));

		String resultado10 = ("El resultado de " + val1 + " y " + val2 + " es: " + valor.menorIgualQue(val1, val2));

		String resultado11 = ("El resultado de " + val1 + " y " + val2 + " es: " + valor.mayorIgualQue(val1, val2));

		// Operadores Logicos
		String resultado12 = ("El resultado de " + v1 + " y " + v2 + " es: " + val.andLogic(v1, v2));
		String resultado13 = ("El resultado de " + v1 + " y " + v2 + " es: " + val.orLogic(v1, v2));
		
		

	}

}
