package deloitte.Academy.lesson.Comparisons;

import java.util.logging.Level;

import java.util.logging.Logger;

/**
 * clase que se encarga de realizar las comparaciones de: =,>,<,>=,<=, a dos
 * numeros enteros.
 * 
 * @author nnavarrete
 *
 */
public class Comparisons {

	private static final Logger LOGGER = Logger.getLogger(Comparisons.class.getName());

	/**
	 * metodo para comparar si dos numeros enteros tienen el mismo valor
	 * 
	 * @param num1 Integer
	 * @param num2 Integer
	 * @return boolean
	 */
	public Boolean valoresIguales(Integer num1, Integer num2) {

		boolean resultado = false;
		try {

			if (num1 == num2) {
				LOGGER.info("Valores iguales");
				resultado = true;
			} else {
				LOGGER.info("Valores diferentes");
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return resultado;

	}

	/**
	 * metodo para verificar si dos numeros tienen diferente valor
	 * 
	 * @param num1 Integer
	 * @param num2 Integer
	 * @return boolean
	 */
	public Boolean valoresDiferentes(Integer num1, Integer num2) {

		boolean resultado = false;
		try {

			if (num1 != num2) {
				LOGGER.info("Son valores diferentes");
				resultado = true;
			} else {
				LOGGER.info("Son valores iguales");
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return resultado;

	}

	/**
	 * metodo para verificar que el primer numero es mayor que el segundo
	 * 
	 * @param num1 Integer
	 * @param num2 Integer
	 * @return boolean
	 */
	public Boolean menorQue(Integer num1, Integer num2) {

		boolean resultado = false;
		try {

			if (num1 < num2) {
				LOGGER.info("El numero" + num1 + " es menor que" + num2);
				resultado = true;
			} else {
				LOGGER.info(num2 + "No es mayor que el numero" + num1);
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return resultado;

	}

	/**
	 * metodo que verifica que el numero dos es menor que el numero uno
	 * 
	 * @param num1 Integer
	 * @param num2 Integer
	 * @return boolean
	 */

	public Boolean mayorQue(Integer num1, Integer num2) {

		boolean resultado = false;
		try {

			if (num1 > num2) {
				LOGGER.info("El numero" + num1 + " es mayor que" + num2);
				resultado = true;
			} else {
				LOGGER.info(num1 + "No es mayor que el numero" + num2);
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return resultado;

	}

	/**
	 * metodo para comparar si el primer numero es menor o igual que el segundo
	 * 
	 * @param num1 Integer
	 * @param num2 Integer
	 * @return boolean
	 */
	public Boolean menorIgualQue(Integer num1, Integer num2) {

		boolean resultado = false;
		try {

			if (num1 <= num2) {
				LOGGER.info("El numero" + num1 + " es menor o igual que" + num2);
				resultado = true;
			} else {
				LOGGER.info("El numero" + num1 + "no es menor que el numero" + num2);
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return resultado;

	}

	/**
	 * metodo que verifica que el primer numero es mayor o igual que el segundo
	 * 
	 * @param num1 Integer
	 * @param num2 Integer
	 * @return boolean
	 */
	public Boolean mayorIgualQue(Integer num1, Integer num2) {

		boolean resultado = false;
		try {

			if (num1 >= num2) {
				LOGGER.info("El numero" + num1 + " es mayor o igual que" + num2);
				resultado = true;
			} else {
				LOGGER.info("El numero" + num1 + " no es mayor que " + num2);
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return resultado;

	}

}
