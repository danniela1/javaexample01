package deloitte.Academy.lesson.Arithmetic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que se encarga de realizar operaciones aritmeticas
 * (suma,resta,multiplicacion,division y modulo) a dos numeros enteros.
 * 
 * @author nnavarrete
 *
 */
public class Arithmetic {

	private static final Logger LOGGER = Logger.getLogger(Arithmetic.class.getName());

	/**
	 * metodo que permite realizar suma entre dos numeros.
	 * 
	 * @param num1 integer
	 * @param num2 integer
	 * @return integer
	 */

	/**
	 * metodo que permite realizar una resta entre dos numeros.
	 * 
	 * @param num1 integer
	 * @param num2 integer
	 * @return integer
	 */

	public Integer resta(Integer num1, Integer num2) {

		Integer resultado = 0;
		try {
			resultado = (num1 - num2);

			LOGGER.info("Resta realizada correctamente ");

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return resultado;

	}

	/**
	 * metodo para realizar una multiplicacion entre dos numeros.
	 * 
	 * @param num1 integer
	 * @param num2 integer
	 * @return integer
	 */

	public Integer multiplicacion(Integer num1, Integer num2) {

		Integer resultado = 0;
		try {
			resultado = (num1 * num2);

			LOGGER.info("Multiplicacion realizada correctamente ");

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return resultado;

	}

	/**
	 * metodo para realizar una division entre dos numeros
	 * 
	 * @param num1 integer
	 * @param num2 integer
	 * @return integer
	 */

	public Integer division(Integer num1, Integer num2) {

		Integer resultado = 0;
		try {
			resultado = (num1 / num2);

			LOGGER.info("Division realizada correctamente ");

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return resultado;

	}

	/**
	 * 
	 * metodo para obtener el modulo entre dos numeros.
	 * 
	 * @param num1 integer
	 * @param num2 integer
	 * @return integer
	 */

	public Integer modulo(Integer num1, Integer num2) {

		Integer resultado = 0;
		try {
			resultado = (num1 % num2);

			LOGGER.info("Modulo obtenido correctamente ");

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return resultado;
	}
}
