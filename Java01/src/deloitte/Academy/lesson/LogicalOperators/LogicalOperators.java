package deloitte.Academy.lesson.LogicalOperators;

import java.util.logging.Level;

import java.util.logging.Logger;

/**
 * clase que se encarga de evaluar dos valores enteros usando operadores logicos (AND, OR). Obteniendo una respuesta 'false' o 'true'.
 * @author nnavarrete
 *
 */
public class LogicalOperators {

	private static final Logger LOGGER = Logger.getLogger(LogicalOperators.class.getName());

	/**
	 * 
	 * Con este metodo, podras escribir expresiones, en las cuales se necesitará que ambas sean correctas para que te regrese el valor 'verdadero'.
	 * @param num1 integer
	 * @param num2 integer
	 * @return boolean
	 */
	public Boolean andLogic(Integer num1, Integer num2) {

		boolean resultado = false;
		try {

			if (num1 < 10 && num2 > 4) {
				LOGGER.info(num1 + " es menor a 10 y " + num2 + " es mayor a 4");
				resultado = true;
			} else {
				LOGGER.info("No cumplen con lo requerido");
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}

		return resultado;

	}

	/**
	 * Este metodo se encarga de evaluar que al menos una de las expresiones sea cierta, para asi retornar un valor 'true'.
	 * @param num1 Integer
	 * @param num2 Integer
	 * @return boolean
	 */
	public Boolean orLogic(Integer num1, Integer num2) {

		boolean resultado = false;
		try {
			if (num1 < 50 || num2 > 2) {
				LOGGER.info(num1 + "es menor a 50 O " + num2 + " es mayor a 2");
				resultado = true;
			} else {
				LOGGER.info("No cumple con lo requerido");
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);
		}
		return resultado;
	}

	
}
